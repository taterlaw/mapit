//
//  AppDelegate.swift
//  MapIt
//
//  Created by Austin Brown on 5/15/20.
//  Copyright © 2020 Ares Software. All rights reserved.
//

// https://benscheirman.com/2019/10/troubleshooting-appkit-file-permissions/

// https://www.raywenderlich.com/450-menus-and-popovers-in-menu-bar-apps-for-macos

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    let home = FileManager.default.homeDirectoryForCurrentUser
    
    let statusItem = NSStatusBar.system.statusItem(withLength:NSStatusItem.squareLength)
    
    let ApplicationMappingData = "ApplicationMappingData"
    
    let ApplicationFolderPermissions = "ApplicationFolderPermissions"
    
    let ApplicationsShortcutData = "ApplicationsShortcutData"
    
    // Symbol, URL
    var menuItemToMapData = [String : String]()
    
    // Symbol, folder permission data
    var folderPermissionsMap = [String : Data]()
    
    // Symbol, shortcut expression
    var shortcutExpressionMap = [String : String]()
    
    let inputPopover = NSPopover()
    
    var validInput = false;
    
    var symbolInput = ""
    
    var shortcutInput = ""
    
    var potentialURL = URL(fileURLWithPath: "")
    
    var potentialDirPermission = Data()
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        if let button = statusItem.button {
            button.image = NSImage(named:NSImage.Name("MapIt_icon"))
        }
        
        constructMenu()
        
        RestoreSavedData()
        
        RepopulateNSMenuItemsFromSave()
        
        inputPopover.contentViewController = NewItemInputViewController.freshController()
        (inputPopover.contentViewController as! NewItemInputViewController).caller = self
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
        
    }
    
    @objc func TogglePopover(_ sender: Any?) {
        if inputPopover.isShown {
            ClosePopover(sender: sender)
        } else {
            ShowPopover(sender: sender)
        }
    }
    
    func ShowPopover(sender: Any?) {
        if let button = statusItem.button {
            inputPopover.show(relativeTo: button.bounds, of: button, preferredEdge: NSRectEdge.minY)
        }
    }
    
    func ClosePopover(sender: Any?) {
        inputPopover.performClose(sender)
    }
    
    func AddNewMappingToList() {
        let newMenuItem = NSMenuItem(title: symbolInput, action: #selector(OpenMappying(_ :)), keyEquivalent: shortcutInput)
        menuItemToMapData.updateValue(potentialURL.absoluteString, forKey: newMenuItem.title)
        folderPermissionsMap.updateValue(potentialDirPermission, forKey: newMenuItem.title)
        statusItem.menu?.addItem(newMenuItem)
        SaveMappedData()
    }
    
    @objc func AddMappingDidPush(_ sender: Any?) {
        UserDefaults.standard.setValue("string", forKey: "hello")
        if let newItemURL = PromptForDirectory() {
            if let permission = GetBookmarkData(for: newItemURL) {
                potentialURL = newItemURL
                potentialDirPermission = permission
                ShowPopover(sender: nil)
            }
        }
    }
    
    @objc func RemoveMapping(_ sender: Any?) {
        print("Remove")
        //SaveMappedData()
    }
    
    @objc func OpenMappying(_ sender: Any?) {
        print("Open Something")
        if let menuItem = sender as? NSMenuItem {
            var folderPath = URL(fileURLWithPath: menuItemToMapData[menuItem.title]!)
            
            if IsDirPermissionDataStale(with: folderPermissionsMap[menuItem.title]!) {
                if let newDirPermission = GetBookmarkData(for: folderPath) {
                    folderPermissionsMap[menuItem.title]! = newDirPermission
                    SaveMappedData()
                }
            }
            
            let _ = folderPath.startAccessingSecurityScopedResource()
            var directoryContents = [URL]()
            var directoryHasContents = false;
            do {
                directoryContents = try FileManager.default.contentsOfDirectory(at: folderPath, includingPropertiesForKeys: nil)
                if directoryContents.count > 0 {
                    directoryHasContents = true
                }
            } catch {
                print(error)
            }
            folderPath.stopAccessingSecurityScopedResource()
            
            if directoryHasContents {
                folderPath.appendPathComponent(directoryContents[0].lastPathComponent)
                // Open directory in Finder window
                NSWorkspace.shared.activateFileViewerSelecting([folderPath])
            } else {
                // Added temp dir/file
                
                // Remove temp dir/file
            }
        }
    }
    
    func constructMenu() {
        let menu = NSMenu()
        
        menu.addItem(NSMenuItem(title: "Add Map", action: #selector(AppDelegate.AddMappingDidPush(_:)), keyEquivalent: "A"))
        menu.addItem(NSMenuItem(title: "Remove Map", action: #selector(AppDelegate.RemoveMapping(_:)), keyEquivalent: "R"))
        menu.addItem(NSMenuItem(title: "Quit", action: #selector(NSApplication.terminate(_:)), keyEquivalent: "q"))
        menu.addItem(NSMenuItem.separator())
        
        statusItem.menu = menu
    }
    
    // Prompt user to grant access to user folder
    private func PromptForDirectory() -> URL? {
        let openPanel = NSOpenPanel()
        openPanel.message = "Choose your user directory"
        openPanel.prompt = "Choose"
        openPanel.allowedFileTypes = ["none"]
        openPanel.allowsOtherFileTypes = false
        openPanel.canChooseFiles = false
        openPanel.canChooseDirectories = true
        
        _ = openPanel.runModal()
        print(openPanel.urls) // this contains the chosen folder
        return openPanel.urls.first
    }
    
    // Obtain directory access permission
    private func GetBookmarkData(for workDir: URL) -> Data? {
        var dirPermission: Data? = nil
        do {
            dirPermission =  try workDir.bookmarkData(options: .withSecurityScope,
                                                      includingResourceValuesForKeys: nil,
                                                      relativeTo: nil)
        } catch {
            print("Failed to save bookmark data for \(workDir)", error)
        }
        return dirPermission
    }
    
    private func IsDirPermissionDataStale(with bookmarkData: Data) -> Bool {
        var isStale = false
        do {
            let url = try URL(resolvingBookmarkData: bookmarkData, options: .withSecurityScope,
                              relativeTo: nil, bookmarkDataIsStale: &isStale)
            print(url.absoluteString)
        } catch {
            print("Error resolving bookmark:", error)
        }
        return isStale
    }
    
    private func SaveMappedData() {
        UserDefaults.standard.set(menuItemToMapData, forKey: ApplicationMappingData)
        UserDefaults.standard.set(folderPermissionsMap, forKey: ApplicationFolderPermissions)
        UserDefaults.standard.set(shortcutExpressionMap, forKey: ApplicationsShortcutData)
    }
    
    // Try to load application data if there is any stored
    private func RestoreSavedData() {
        // Has to be .object call to work and force casted as such
        if let savedMenuItemData = UserDefaults.standard.object(forKey: ApplicationMappingData) {
            menuItemToMapData = savedMenuItemData as! [String : String]
        }
        
        if let savedFolderPermissions = UserDefaults.standard.object(forKey: ApplicationFolderPermissions) {
            folderPermissionsMap = savedFolderPermissions as! [String : Data]
        }
        
        if let savedShortcuts = UserDefaults.standard.object(forKey: ApplicationsShortcutData) {
            shortcutExpressionMap = savedShortcuts as! [String : String]
        }
    }
    
    private func RepopulateNSMenuItemsFromSave() {
        for key in menuItemToMapData.keys {
            let newMenuItem = NSMenuItem(title: key, action: #selector(OpenMappying(_ :)), keyEquivalent: "X")
            statusItem.menu?.addItem(newMenuItem)
        }
    }
}

extension FileManager {
    func urls(for directory: FileManager.SearchPathDirectory, skipsHiddenFiles: Bool = true ) -> [URL]? {
        let documentsURL = urls(for: directory, in: .userDomainMask)[0]
        let fileURLs = try? contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil, options: skipsHiddenFiles ? .skipsHiddenFiles : [] )
        return fileURLs
    }
}
