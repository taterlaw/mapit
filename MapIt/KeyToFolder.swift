//
//  KeyToFolderMap.swift
//  MapIt
//
//  Created by Austin Brown on 5/15/20.
//  Copyright © 2020 Ares Software. All rights reserved.
//

// https://www.hackingwithswift.com/books/ios-swiftui/making-someone-elses-class-conform-to-codable

import Foundation
import Cocoa

public class KeyToFolder : Codable {
    /*public func encode(with coder: NSCoder) {
        coder.encode(symbol, forKey: CodingKeys.symbol)
        coder.encode(folderPath, forKey: CodingKeys.folderPathKey)
        coder.encode(folderPermission, forKey: CodingKeys.folderPermisionKey)
        coder.encode(menuItemIndex, forKey: CodingKeys.menuItemIndexKey)
    }
    
    public required init?(coder: NSCoder) {
        symbol = try coder.decodeObject(String.self, forKey: CodingKeys.symbolKey)
        folderPath = try coder.decdecodeObjectode(URL.self, forKey: CodingKeys.folderPathKey)
        folderPermission = try coder.decodecodeObjectde(Data.self, forKey: CodingKeys.folderPermisionKey)
        menuItemIndex = try coder.decodeObject(Int.self, forKey: CodingKeys.menuItemIndexKey)
    }*/
    
    
    enum CodingKeys: CodingKey {
        case symbolKey, folderPathKey, folderPermisionKey, menuItemIndexKey
    }
    
    var symbol: String
    var folderPath: URL
    var folderPermission: Data
    var menuItemIndex: Int
    
    init() {
        symbol = ""
        folderPath = URL(fileURLWithPath: "")
        folderPermission = Data()
        menuItemIndex = -1
    }
    
    init(title name: String, mappedTo path: URL, with permision: Data, at item: Int) {
        symbol = name
        folderPath = path
        folderPermission = permision
        menuItemIndex = item
    }
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        symbol = try container.decode(String.self, forKey: CodingKeys.symbolKey)
        folderPath = try container.decode(URL.self, forKey: CodingKeys.folderPathKey)
        folderPermission = try container.decode(Data.self, forKey: CodingKeys.folderPermisionKey)
        menuItemIndex = try container.decode(Int.self, forKey: CodingKeys.menuItemIndexKey)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(symbol, forKey: CodingKeys.symbolKey)
        try container.encode(folderPath, forKey: CodingKeys.folderPathKey)
        try container.encode(folderPermission, forKey: CodingKeys.folderPermisionKey)
        try container.encode(menuItemIndex, forKey: CodingKeys.menuItemIndexKey)
    }
}
