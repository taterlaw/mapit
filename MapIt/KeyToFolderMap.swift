//
//  KeyToFolderMap.swift
//  MapIt
//
//  Created by Austin Brown on 5/15/20.
//  Copyright © 2020 Ares Software. All rights reserved.
//

import Foundation

class KeyToFolderPath {
    var key: String
    var folderPath: URL
    
    init() {
        key = ""
        folderPath = URL(fileURLWithPath: "")
    }
    
    init(populateWith key: String, mappedTo path: String) {
        self.key = key
        folderPath = URL(fileURLWithPath: path)
    }
}
