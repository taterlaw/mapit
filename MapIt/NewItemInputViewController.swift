//
//  NewItemInputViewController.swift
//  MapIt
//
//  Created by Austin Brown on 5/17/20.
//  Copyright © 2020 Ares Software. All rights reserved.
//

import Cocoa

class NewItemInputViewController: NSViewController {
    
    var caller = AppDelegate()
    
    @IBOutlet weak var DisplaySymbolLabel: NSTextField!
    
    @IBOutlet weak var ShortcutLabel: NSTextField!
    
    @IBOutlet weak var SymbolInputField: NSTextField!
    
    @IBOutlet weak var ShortcutInputField: NSTextField!
    
    @IBAction func AddDidTouch(_ sender: Any) {
        caller.symbolInput = SymbolInputField.stringValue
        caller.shortcutInput = ShortcutInputField.stringValue
        caller.ClosePopover(sender: caller)
    }
    
    @IBAction func CancelDidTouch(_ sender: Any) {
        caller.ClosePopover(sender: caller)
    }
    
    var symbol = ""
    var shortcutExpression = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
}

extension NewItemInputViewController {
    // MARK: Storyboard instantiation
    static func freshController() -> NewItemInputViewController {
        // Get a reference to main storyboard
        let storyboard = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
        // Create a scene ID that matches the one you set just before
        let identifier = NSStoryboard.SceneIdentifier("NewItemInputViewController")
        // Instantiate NewItemInputViewController and return it
        guard let viewcontroller = storyboard.instantiateController(withIdentifier: identifier) as? NewItemInputViewController else {
            fatalError("Why cant i find NewItemInputViewController? - Check Main.storyboard")
        }
        return viewcontroller
    }
}
